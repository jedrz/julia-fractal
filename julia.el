(defun julia-black (w h it cr ci)
  "Rysuje fraktal Julii o wymiarach W x H.
Oblicza funkcję maksymalnie IT razy.
Część rzeczywista stałej to CR, urojona CI."
  (pop-to-buffer (get-buffer-create "*julia*"))
  (fundamental-mode)
  (erase-buffer)
  (insert (format "P1\n%d %d\n" w h))
  (dotimes (y h)
    (dotimes (x w)
      ;; Jak obliczam początkowe współrzędne z wymiarów obrazka (np. dana
      ;; współrzędna od 0 do `w'):
      ;; - przesuń współrzędną o połowę wymiaru (długości lub szerokości),
      ;; - i podziel to co otrzymałeś też przez połowę wymiaru.
      ;; W taki sposób z liczby z przedziału [0, wymiar] otrzymujemy
      ;; wynik w przedziale [-1,1].
      (let* ((zr (/ (- x (/ w 2.0)) (/ w 2.0)))
             ;; W tym zmieniamy znak bo y musi mieć inny znak od iksa.
             (zi (- (/ (- y (/ h 2.0)) (/ h 2.0))))
             (yes-or-no (dotimes (i it 1)
                          (if (> (+ (* zi zi) (* zr zr)) 4)
                              (return 0)
                            (psetq zr (+ (* zr zr) (- (* zi zi)) cr)
                                   zi (+ (* zr zi 2) ci))))))
        (insert (concat (number-to-string yes-or-no) " "))))
    (insert "\n"))
  (image-mode))

(defun julia-color (width height iterations c-real c-imag map-color)
  "Rysuje fraktal Julii o wymiarach WIDTH x HEIGHT w formacie PPM.

Oblicza funkcję maksymalnie ITERATIONS razy.
Część rzeczywista stałej to C-REAL, urojona C-IMAG.

Do zmapowania koloru wywołuje funkcję MAP-COLOR z argumentem z zakresu [0, 1]."
  (pop-to-buffer (get-buffer-create "*julia*"))
  (fundamental-mode)
  (erase-buffer)
  (set-buffer-multibyte nil)
  (insert (format "P6\n%d %d\n255\n" width height))
  (dotimes (y height)
    (dotimes (x width)
      ;; Jak obliczam początkowe współrzędne z wymiarów obrazka (np. dana
      ;; współrzędna od 0 do `width'):
      ;; - przesuń współrzędną o połowę wymiaru (długości lub szerokości),
      ;; - i podziel to co otrzymałeś 1/4 wymiaru.
      ;; W taki sposób z liczby z przedziału [0, wymiar] otrzymujemy
      ;; wynik w przedziale [-2, 2].
      (let* ((zr (/ (- x (/ width 2.0)) (/ width 4.0)))
             ;; W tym zmieniamy znak bo y musi mieć inny znak od iksa.
             (zi (- (/ (- y (/ height 2.0)) (/ height 4.0))))
             (v (dotimes (i iterations iterations)
                  (if (> (+ (* zi zi) (* zr zr)) 4)
                      (return i)
                    (psetq zr (+ (* zr zr) (- (* zi zi)) c-real)
                           zi (+ (* zr zi 2) c-imag))))))
        ;; (insert-char (floor (* 256 (/ v 1.0 iterations))) 3)
        (funcall map-color (/ v 1.0 iterations))))
    ;; Postęp obliczeń.
    (message "%d/%d" y height))
  (image-mode))

;;; Poniższe funkcje wypisują w buforze jakiś kolor w formacie P6.

(defun julia-mapblack (v)
  "Mapuje V na kolor w skali szarości."
  (insert-char (floor (* 255 (- 1 v))) 3))

(defun julia-mapcolor (v)
  "Mapuje V na jakiś kolor (w odcieniu niebieskim)."
  (loop for i in '(0.33 0.66 0.99) do
        (insert-char (floor (* 256 (* i v))) 1)))

;;; Nawet fajne kolory.
(defun julia-mapcolor-modulo (v)
  (loop for i in '(3 1 2) do
        (insert-char (floor (* 255 (mod (* i v) 1))) 1)))

(defun julia-mapcolor-nice (v)
  "Given a value between 0 and 1.0, insert a P6 color."
  (dotimes (i 3)
    (insert-char (floor (* 256 (min 0.99 (sqrt (* (- 3 i) v))))) 1)))

(julia-black 600 600 30 -0.73 0.19)
(julia-black 400 400 100 -0.1 0.65)
(julia-black 400 400 20 -0.1 0.65)

(julia-color 600 400 32 -0.73 0.19 'julia-mapcolor)
(julia-color 400 400 32 -0.1 0.65 'julia-mapcolor)
(julia-color 200 200 32 -0.1 0.65 'julia-mapblack)

(julia-color 200 200 32 0.1 -0.65 'julia-mapcolor-modulo)
(julia-color 400 400 32 -0.1 0.65 'julia-mapcolor-modulo)
(julia-color 300 300 32 -0.4 0.6 'julia-mapcolor-modulo)
(julia-color 300 300 32 -0.8 0.156 'julia-mapcolor-modulo)
