    .data

tmp_buffer:
    .space 8

filename:
    .space 100

bmp_magic_code:
    .ascii "BM"
bmp_four_zeros:
    .byte 0, 0, 0, 0
bmp_eigth_zeros:
    .byte 0, 0, 0, 0, 0, 0, 0, 0
bmp_offset:
    .byte 54, 0, 0, 0
bmp_DIB_size:
    .byte 40, 0, 0, 0
bmp_planes:
    .byte 1, 0
bmp_bit_count:
    .byte 24, 0

black:
    .byte 0, 0, 0
white:
    .byte 255, 255, 255

zero_byte:
    .byte 0

two:
    .double 2.0

four:
    .double 4.0

    .text
    .globl main

main:
    ## nazwa pliku (z .bmp)
    la $a0, filename
    li $a1, 100
    li $v0, 8
    syscall
    ## usun '\n'
    ## $t0 - wskaznik na bufor
    ## $t1 - znak pod wskaznikiem
    la $t0, filename
filename_l_clean:
    lbu $t1, ($t0)
    beq $t1, '\n', filename_clean
    addiu $t0, $t0, 1
    j filename_l_clean
filename_clean:
    sb $zero, ($t0)

    ## szerokosc
    li $v0, 5
    syscall
    move $s0, $v0

    ## wysokosc
    li $v0, 5
    syscall
    move $s1, $v0

    ## liczba iteracji
    li $v0, 5
    syscall
    move $s2, $v0

    ## czesc rzeczywista stalej
    li $v0, 7
    syscall
    mov.d $f20, $f0

    ## czesc urojona stalej
    li $v0, 7
    syscall
    mov.d $f22, $f0

    ## przygotuj argumenty do wywolania
    move $a0, $s0
    move $a1, $s1
    move $a2, $s2
    la $a3, filename
    mov.d $f12, $f20
    mov.d $f14, $f22

    jal julia

    li $v0, 10
    syscall

### julia:
### Rysuje fraktal julli.

### Argumenty:
### - $a0 - szerokosc (kopia w $s0)
### - $a1 - wysokosc (kopia w $s1)
### - $a2 - liczba iteracji (kopia w $s2)
### - $a3 - adres do nazwy pliku (kopia w $s3)
### - $f12 (double) - czesc rzeczywista stalej (kopia w $f20)
### - $f14 (double) - czesc urojona stalej (kopia w $f22)

### Uzywane rejestry:
### - $t0 - aktualna rozpatrywana szerokosc
### - $t1 - aktualna wysokosc (do iterowania)
### - $t2 - pozostala liczba iteracji do wykonania
### - $t3 - deskryptor pliku
### - $t4 - (szerokosc * 3) % 4 - do wypisania zerowych bajtow na koncu linii
### - $t5 - stala 4 (poczatkowo do powyzszego dzielenia, pozniej uzywany
###         do wypisania zer na koncu linii)
### - $f24 - czesc rzeczywista rozpatrywanego punktu
### - $f26 - czesc urojona rozpatrywanego punktu
### - $f28 - tymczasowy rejestr do zapisania wyniku miedzy wywolaniami next_*

julia:
    ## zachowaj uzywane rejestry
    subu $sp, $sp, 60
    sw $ra, 56($sp)
    sw $s3, 52($sp)
    sw $s2, 48($sp)
    sw $s1, 44($sp)
    sw $s0, 40($sp)
    s.s $f29, 36($sp)
    s.s $f28, 32($sp)
    s.s $f27, 28($sp)
    s.s $f26, 24($sp)
    s.s $f25, 20($sp)
    s.s $f24, 16($sp)
    s.s $f23, 12($sp)
    s.s $f22, 8($sp)
    s.s $f21, 4($sp)
    s.s $f20, ($sp)

    ## przygotuj rejestry
    move $s0, $a0
    move $s1, $a1
    move $s2, $a2
    move $s3, $a3
    mov.d $f20, $f12
    mov.d $f22, $f14

    ## oblicz liczbe bajtow do wypelnienia na koncu kazdej linii mapy pikseli
    move $t4, $s0               # szerokosc
    mulu $t4, $t4, 3            # szerokosc * 3
    ## (szerokosc * 3) % 4
    li $t5, 4
    divu $t4, $t5
    mfhi $t4                    # zapisz reszte
    subu $t4, $t5, $t4          # 4 - reszta by uzyskac pozadana wartosc
    bne $t4, 4, julia_rest_good # ok
    ## reszta wyniosla zero przed poprawka, czyli nie musimy dopelniac zerami
    ## konca linii pikseli, nalezy to poprawic
    subu $t4, $t4, 4
julia_rest_good:
    ## otworz plik do zapisu
    li $v0, 13
    la $a0, ($s3)
    li $a1, 1
    li $a2, 0
    syscall
    move $t3, $v0               # zachowaj deskryptor pliku

    ## Stworz naglowek pliku
    ## Magic code
    li $v0, 15
    move $a0, $t3
    la $a1, bmp_magic_code
    li $a2, 2
    syscall
    ## rozmiar calego pliku (4 bajty)
    ## = 54 (rozmiar naglowka) + (liczba bajtow z mapy pikseli wyrownana do
    ## liczby podzielnej przez 4 w kazdym wierszu)
    move $a0, $s0               # szerokosc
    mulu $a0, $a0, 3            # szerokosc * 3
    addu $a0, $a0, $t4          # szerokosc * 3 + (szerokosc * 3) % 4
    mulu $a0, $a0, $s1          # ... * wysokosc
    addu $a0, $a0, 54           # ... + 54
    ## konwertuj do napisu
    la $a1, tmp_buffer
    li $a2, 4
    jal int_to_string
    ## zapisz rozmiar do pliku
    li $v0, 15
    move $a0, $t3
    la $a1, tmp_buffer
    li $a2, 4
    syscall
    ## zarezerwowany (4 bajty)
    li $v0, 15
    move $a0, $t3
    la $a1, bmp_four_zeros
    li $a2, 4
    syscall
    ## offset (4 bajty)
    li $v0, 15
    move $a0, $t3
    la $a1, bmp_offset
    li $a2, 4
    syscall
    ## liczba bajtow w DIB (40) (4 bajty)
    li $v0, 15
    move $a0, $t3
    la $a1, bmp_DIB_size
    li $a2, 4
    syscall
    ## szerokosc w pikselach (rzeczywista bez wyrownania) (4 bajty)
    move $a0, $s0               # szerokosc
    la $a1, tmp_buffer
    li $a2, 4                   # do ilu znakow wypelnic bufor
    jal int_to_string
    li $v0, 15
    move $a0, $t3
    la $a1, tmp_buffer
    li $a2, 4
    syscall
    ## wysokosc w pikselach (rzeczywista bez wyrownania) (4 bajty)
    move $a0, $s1               # wysokosc
    la $a1, tmp_buffer
    li $a2, 4                   # do ilu znakow wypelnic bufor
    jal int_to_string
    li $v0, 15
    move $a0, $t3
    la $a1, tmp_buffer
    li $a2, 4
    syscall
    ## planes (2 bajty)
    li $v0, 15
    move $a0, $t3
    la $a1, bmp_planes
    li $a2, 2
    syscall
    ## liczba bitow na piksel (2 bajty)
    li $v0, 15
    move $a0, $t3
    la $a1, bmp_bit_count
    li $a2, 2
    syscall
    ## kompresja (4 bajty)
    li $v0, 15
    move $a0, $t3
    la $a1, bmp_four_zeros
    li $a2, 4
    syscall
    ## rozmiar obrazka w bajtach (mozna pominac ze wzgledu na brak kompresji)
    ## (4 bajty)
    li $v0, 15
    move $a0, $t3
    la $a1, bmp_four_zeros
    li $a2, 4
    syscall
    ## rozdzielczosc (mozna na 0) (4 + 4 bajty)
    li $v0, 15
    move $a0, $t3
    la $a1, bmp_eigth_zeros
    li $a2, 8
    syscall
    ## ustawienia kolorow (mozna na 0) (4 + 4 bajty)
    li $v0, 15
    move $a0, $t3
    la $a1, bmp_eigth_zeros
    li $a2, 8
    syscall

    ## zacznij od zerowego igreka (linie w bitmapie zapisane sa
    ## od dolu do gory)
    move $t1, $zero
julia_l_height:
    beq $t1, $s1, julia_clean
    ## zaczynamy od zerowego iksa
    li $t0, 0
julia_l_width:
    beq $t0, $s0, julia_l_height_post
    ## przygotuj rejestr z liczba iteracji
    move $t2, $s2
    ## oblicz czesc rzeczywista kolejnego punktu do rozpatrzenia z plaszczyzny zespolonej
    move $a0, $t0
    move $a1, $s0
    jal start_coord
    mov.d $f24, $f0              # zapamietaj zwrocony wynik
    ## oblicz czesc urojona kolejnego punktu do rozpatrzenia z plaszczyzny
    ## zespolonej
    move $a0, $t1
    move $a1, $s1
    jal start_coord
    mov.d $f26, $f0              # zapamietaj zwrocony wynik
julia_l_it:
    ## sprawdz czy maksymalna liczba iteracji nie zostala osiagnieta
    ## jesli tak to ten punkt nalezy do zbioru
    beqz $t2, julia_l_map_color # zmapuj go na czarno
    ## sprawdz czy rozpatrywany punkt spelnia warunek z modulem
    mov.d $f12, $f24
    mov.d $f14, $f26
    jal check_condition
    ## jesli zero to punkt nie nalezy do zbioru, zmapuj go na jakis kolor
    ## (nie czarny, im jasniejszy tym nie spelnil warunku po mniejszej liczbie
    ## iteracji)
    beqz $v0, julia_l_map_color
    ## a jesli nie to licz kolejne wartosci ze wzoru
    ## oblicz kolejna czesc rzeczywista
    mov.d $f12, $f24
    mov.d $f14, $f26
    subu $sp, $sp, 8            # musimy przekazac trzeci parametr przez stos
    s.s $f21, 4($sp)
    s.s $f20, ($sp)
    jal next_real
    ## wynik zapisz do tymczasowego rejestru, aby nie nadpisac starej wartosci
    ## ktora jest potrzebna do poprawnego obliczenia kolejnej czesci urojonej
    mov.d $f28, $f0
    ## oblicz kolejna czesc urojona
    mov.d $f12, $f24
    mov.d $f14, $f26
    s.s $f23, 4($sp)            # zapisz trzeci parametr na stosie
    s.s $f22, ($sp)
    jal next_imag
    ## odtworz wskaznik stosu
    addiu $sp, $sp, 8
    ## teraz nadpisz rejestry zwroconymi wartosciami
    mov.d $f24, $f28
    mov.d $f26, $f0
    ## iteracja wykonana
    subu $t2, $t2, 1
    j julia_l_it
julia_l_map_color:
    move $a0, $t2
    move $a1, $s2
    move $a2, $t3
    jal map_grey
    ## jal map_red
julia_l_width_post:
    addiu $t0, $t0, 1
    j julia_l_width
julia_l_height_post:
    ## dopelnij koniec linii w bitmapie zerami do dlugosci podzielnej przez 4
    move $t5, $t4               # nie nadpisuj ciezko obliczonej reszty
julia_l_fill:
    beqz $t5, julia_l_fill_end
    ## zapisz pojedyncze zero do pliku
    li $v0, 15
    move $a0, $t3
    la $a1, zero_byte
    li $a2, 1
    syscall
    subu $t5, $t5, 1
    j julia_l_fill
julia_l_fill_end:
    addiu $t1, $t1, 1           # zwieksz wysokosc
    j julia_l_height

julia_clean:
    ## zamknij plik
    li $v0, 16
    move $a0, $t3
    syscall

    ## odtworz zapisane rejestry
    l.s $f20, ($sp)
    l.s $f21, 4($sp)
    l.s $f22, 8($sp)
    l.s $f23, 12($sp)
    l.s $f24, 16($sp)
    l.s $f25, 20($sp)
    l.s $f26, 24($sp)
    l.s $f27, 28($sp)
    l.s $f28, 32($sp)
    l.s $f29, 36($sp)
    lw $s0, 40($sp)
    lw $s1, 44($sp)
    lw $s2, 48($sp)
    lw $s3, 52($sp)
    lw $ra, 56($sp)
    addiu $sp, $sp, 60

    jr $ra


### map_grey:
### Mapuje liczbę wykonanych iteracji na kolor w odcieniu szarości
### i zapisuje do pliku.
### W szczególności, gdy zostały wykonane wszystkie iteracje
### kolor będzie czarny, żadna - biały.

### Argumenty:
### - $a0 - liczba pozostałych do wykonania iteracji
### - $a1 - maksymalna liczba iteracji
### - $a2 - deskryptor pliku

### Uzywane rejestry/bufory:
### - $t9 - adres bufora
### - tmp_buffer - do zapisu koloru do pliku

map_grey:
    ## poz-it * 255 / max-it
    mulu $a0, $a0, 255
    divu $a0, $a1
    mflo $a0
    ## zapisz 3 obliczone liczby w buforze
    la $t9, tmp_buffer
    sb $a0, ($t9)
    sb $a0, 1($t9)
    sb $a0, 2($t9)
    ## zapisz bufor do pliku
    li $v0, 15
    move $a0, $a2
    la $a1, tmp_buffer
    li $a2, 3
    syscall
    jr $ra


### map_red:
### Mapuje liczbę wykonanych iteracji na kolor w odcieniu czerwonym
### i zapisuje do pliku.
### W szczególności, gdy zostały wykonane wszystkie iteracje
### kolor będzie czerowny, żadna - bialy.

map_red:
    ## poz-it * 255 / max-it
    mulu $a0, $a0, 255
    divu $a0, $a1
    mflo $a0
    ## zapisz 2 obliczone liczby w buforze
    la $t9, tmp_buffer
    sb $a0, ($t9)
    sb $a0, 1($t9)
    ## skladowa czerwona niech zawsze wynosi 255
    li $a0, 255
    sb $a0, 2($t9)
    ## zapisz bufor do pliku
    li $v0, 15
    move $a0, $a2
    la $a1, tmp_buffer
    li $a2, 3
    syscall
    jr $ra


### check_condition:
### Sprawdza czy z_n spelnia warunek |z_n| <= 2.
### Tak naprawde sprawdza, czy |z_n|^2 <= 4.
### Zwraca 1 jesli warunek jest spelniony lub 0 gdy nie.

### Argumenty:
### - $f12 (double) - czesc rzeczywista liczby
### - $f14 (double) - czesc urojona liczby

### Uzywane rejestry:
### - $f4 - suma kwadratow
### - $f6 - czesc rzeczywista do kwadratu
### - $f8 - czesc urojona do kwadratu
### - $f10 - stala 4
### - $t9 - rejestr do zaladowania adresu stalej

check_condition:
    ## podnies do kwadratu czesc rzeczywista i zespolona
    mul.d $f6, $f12, $f12
    mul.d $f8, $f14, $f14
    ## oblicz sume
    add.d $f4, $f6, $f8
    ## zaladuj stala 4 do rejestru $f10
    la $t9, four
    l.d $f10, ($t9)
    ## porownaj
    c.le.d $f4, $f10
    bc1t check_condition_true
    ## warunek niespelniony
    li $v0, 0
    jr $ra
check_condition_true:
    ## warunek spelniony
    li $v0, 1
    jr $ra


### start_coord:
### Zwraca startowa wspolrzedna (x lub y) w przedziale [-2, 2] dla
### rozpatrywanego wymiaru (od 0 do podanego wymiaru).

### Jak obliczam początkowe współrzędne (np. dana współrzędna od 0 do `width'):
### - przesuń współrzędną o połowę wymiaru (długości lub szerokości),
### - i podziel to co otrzymałeś też przez 1/4 wymiaru.
### W taki sposób z liczby z przedziału [0, wymiar] otrzymujemy
### wynik w przedziale [-2, 2].

### Argumenty:
### $a0 - aktualny x lub y
### $a1 - maksymalny wymiar

### Uzywane rejestry:
### $f4 - skonwertowany x lub y do double
### $f6 - skonwertowana szerokosc do double
### $f8 - stala 2
### $t9 - rejestr do zaladowania adresu stalej

start_coord:
    ## skonwertuj wspolrzedne do doubli
    mtc1 $a0, $f4
    cvt.s.w $f4, $f4
    cvt.d.s $f4, $f4
    mtc1 $a1, $f6
    cvt.s.w $f6, $f6
	cvt.d.s $f6, $f6
    ## zaladuj stala 2
    la $t9, two
    l.d $f8, ($t9)
    ## podziel wymiar przez 2
    div.d $f6, $f6, $f8
    ## przesun wspolrzedna o polowe wymiaru
    sub.d $f4, $f4, $f6
    ## podziel przesunieta wspolrzedna przez polowe wymiaru
    div.d $f4, $f4, $f6
    ## i pomnoz przez 2 (by otrzymac liczbe z zakresu [-2, 2])
    mul.d $f4, $f4, $f8
    ## zwroc wynik
    mov.d $f0, $f4
    jr $ra


### next_real:
### Oblicza kolejna czesc rzeczywista liczby (wzor: z_n = z_(n-1)^2 + c)  wg wzoru:
### nowy_x = x^2 - y^2 + Re(c)

### Argumenty:
### - $f12 - dotychczasowa czesc rzeczywista liczby
### - $f14 - dotychczasowa czesc urojona liczby
### - na stosie (double) - czesc rzeczywista stalej c

### Uzywane rejestry:
### - $f18 - do wczytania trzeciego parametru (czesc rzeczywista)

next_real:
    ## wczytaj trzeci parametr ze stosu
    l.s $f19, 4($sp)
    l.s $f18, ($sp)
    ## podnies do kwadratu x, y
    mul.d $f12, $f12, $f12
    mul.d $f14, $f14, $f14
    ## zapisz roznice kwadratow w rejestrze wynikowym
    sub.d $f0, $f12, $f14
    ## dodaj czesc rzeczywista
    add.d $f0, $f0, $f18
    jr $ra


### next_imag:
### Oblicza kolejna czesc urojona liczby (wzor: z_n = z_(n-1)^2 + c)  wg wzoru:
### nowy_y = 2 * x * y + Im(c)

### Argumenty:
### - $f12 - dotychczasowa czesc rzeczywista liczby
### - $f14 - dotychczasowa czesc urojona liczby
### - na stosie (double) - czesc urojona stalej c

### Rejestry:
### - $f4 - stala 2
### - $t9 - do zaladowania adresu stalej

### Uzywane rejestry:
### - $f18 - do wczytania trzeciego parametru (czesc urojona)

next_imag:
    ## wczytaj trzeci parametr ze stosu
    l.s $f19, 4($sp)
    l.s $f18, ($sp)
    ## przemnoz x, y i zapisz w rejestrze wynikowym
    mul.d $f0, $f12, $f14
    ## zaladuj stala
    la $t9, two
    l.d $f4, ($t9)
    ## przemnoz wynik przez 2
    mul.d $f0, $f0, $f4
    ## dodaj czesc urojona stalej
    add.d $f0, $f0, $f18
    jr $ra


### int_to_string:
### Konwertuje liczbe (bez znaku) do stringa w systemie 256 (dwie cyfry
### zajmuja jeden bajt) w odwrotnej kolejnosci.
### Wypelnia bufor zerami do maksymalnie $a2 znakow (napis moze nie konczyc
### sie zerem! lub przekroczyc maksymalny podany rozmiar ($a2)).

### Argumenty:
### - $a0 - int do przekonwertowania
### - $a1 - adres do bufora
### - $a2 - do ilu bajtow wypelnic zerami

### Uzywane rejestry:
### - $t8 - stala 256
### - $t9 - reszta z dzielenia

int_to_string:
    li $t8, 256                 # zaladuj stala do dzielenia
int_to_string_loop:
    beqz $a0, int_to_string_fill # wynik dzielenia - zero - dopelnij zerami
    divu $a0, $t8               # wynik w LO, reszta w HI
    mfhi $t9                    # zapisz reszte w $t9
    mflo $a0                    # zapisz wynik dzielenia w $a0
    sb $t9, ($a1)               # i zapisz w buforze
    addiu $a1, $a1, 1           # przesun wskaznik bufora
    subu $a2, $a2, 1            # jedno zero mniej do wypelnienia
    j int_to_string_loop
int_to_string_fill:
    ## wpisz $a2 zer do bufora
    blez $a2, int_to_string_end # sprawdz czy $a2 <= 0 (<= gdyz dlugosc
                                # skonwertowanej liczby moze byc dluzsza niz $a2)
    sb $zero, ($a1)             # wpisz zero
    addiu $a1, $a1, 1           # przesun wskaznik bufora
    subu $a2, $a2, 1            # jedno zero mniej do wypelnienia
    j int_to_string_fill
int_to_string_end:
    jr $ra                      # wroc do wywolania
